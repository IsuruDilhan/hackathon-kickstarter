const express = require('express');
const app = express();
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3000;
const router = express.Router();
const indexRoutes = require('./routes/index');
const mongooseCreds = require('./config/mongoose');
const bodyParser = require('body-parser');

mongoose.connect(mongooseCreds);

app.use(function (req, res, next) {
	res.setHeader('X-Powered-By', 'HACKATHON');
	next();
});

app.use(router);

app.use('/', express.static('src'));
indexRoutes(router, bodyParser, mongoose);

app.listen(PORT, function() {
	console.log('server running on port', PORT);
});