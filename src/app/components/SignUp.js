import React from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import 'whatwg-fetch';

const style = {
  height: 100,
  width: 100,
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
};

validateStatusText(text) => {
  if(text.length > 250) {
    return {
      "error": "*status is too long"
    }
  }
  else if(text === "") {
    return {
      "error": "*status cannot be empty"
    }
  }
  else {
    return true;
  }
};

export class SignUp extends React.Component{
	constructor(props) {    
	    super(props);
	    this.state = {
	            fName: fName,
	            lName: lName
	    };
	},

	add() {

	    let val = true;
	    let firstName = this.refs.firstName.getValue();
	    let lastName = this.refs.lastName.getValue();
	          
	    if(validateStatusText(firstName).error) {
	      this.setState({
	        fName: validateStatusText(firstName).error
	      });
	      val = false;
	    } 
	    else {

	        let SignUpData = {
	        	firstName: firstName,
	        };
	        ///ActivityfeedAction._addStatus(statusData);


	        fetch('/total',{
	        	 	method: 'POST',
  					body: firstName
	        })
			  .then(function(response) {
			    return response.text()
			  }).then(function(body) {
			    document.body.innerHTML = body
			  })
			  .catch(function(err) {

			  });

	      this.setState({
	        fName: ''
	      });
	    }
	    this.clearText();
  

        /*this.setState({
        	fname: this.state.fname,
        	lname: this.state.lname,
        });  */
    },


	clearText() {
	    document.getElementById('fn').value = "";
	    document.getElementById('ln').value = "";
	},

	render() {
		return (
			  <div>
			    <Paper style={style} zDepth={1} rounded={false} />
			    <TextField 
			      hintText="First Name"
			      floatingLabelText="First Name"
			      errorText={this.state.fName} 
			      ref="firstName" 
			      id="fn"/>
			    <br/>
			    <TextField 
			      hintText="Last Name"
			      floatingLabelText="Last Name"
			      errorText={this.state.lName} 
			      ref="lastName" 
			      id="ln"/>
			    <br/>
			    <FlatButton label="Primary" primary={true} onClick={this.add}/>
			  </div>
			);
	}
}

SignUp.propTypes = { fname: React.PropTypes.string };
//SignUp.defaultProps = { fname: '' };
